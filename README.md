## Installation ##
To install this plugin, drag and drop it into your server's plugins folder. It was tested under the latest Spigot 1.8 build (as of July 2017).

## Config ##
You can configure the plugin in the config.yml file the the folder plugins/ZortixPvPShop of your server. The config has two parts: The configuration of the database system to be used and the configuration of the messages that are being displayed ingame.

### The two database backends ###
First, you will see a point named "backend" with a default value od "sqlite". If you have no idea about databases, leave it like it is. If you have a MySQL server up and running, go ahead and change this value to "mysql", as it will increase the speed a query will be processed with. If you choose to switch to MySQL as your database backend, you have to configure it below. Go ahead end enter your MySQL account credentials. These will be stored only in that config file and will stay there. The developer of this plugin (Max_Plays) is not to be held responsible for any damages that might be caused by this plugin. By downloading this plugin, you agree to the statement before this sentence.
After configuring the database, you can change the messages that will be displayed ingame.

Have fun with the plugin and contact me if you find any bugs. Also, feel free to fork this repository and submit a pull request if you'd like to add in any changes/fixes.