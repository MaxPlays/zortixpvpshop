package me.MaxPlays.ZortixPvPShop.main;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import me.MaxPlays.ZortixPvPShop.commands.CommandPoints;
import me.MaxPlays.ZortixPvPShop.commands.CommandShop;
import me.MaxPlays.ZortixPvPShop.commands.CommandStats;
import me.MaxPlays.ZortixPvPShop.listeners.ChatListener;
import me.MaxPlays.ZortixPvPShop.listeners.ClickListener;
import me.MaxPlays.ZortixPvPShop.listeners.DeathListener;
import me.MaxPlays.ZortixPvPShop.listeners.JoinListener;
import me.MaxPlays.ZortixPvPShop.listeners.PotionListener;
import me.MaxPlays.ZortixPvPShop.shop.Items;
import me.MaxPlays.ZortixPvPShop.shop.Shop;
import me.MaxPlays.ZortixPvPShop.util.PlayerManager;
import me.MaxPlays.ZortixPvPShop.util.SQL;
import me.MaxPlays.ZortixPvPShop.util.Vars;

public class Main extends JavaPlugin{

	public static SQL sql;
	public static Main plugin;
	
	public void onEnable(){
		plugin = this;
		config();
		doSQL();
		
		getCommand("points").setExecutor(new CommandPoints());
		getCommand("stats").setExecutor(new CommandStats());
		getCommand("shop").setExecutor(new CommandShop());
		
		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(new JoinListener(), this);
		pm.registerEvents(new ChatListener(), this);
		pm.registerEvents(new DeathListener(), this);
		pm.registerEvents(new ClickListener(), this);
		pm.registerEvents(new PotionListener(), this);
		
		new Timer();
		
		new Items();
		new Shop();
	}
	public void onDisable(){
		sql.disconnect();
	}
	
	
	private void config(){
		getConfig().options().copyDefaults(true);
		saveConfig();
		new Vars(getConfig());
	}
	private void doSQL(){
		if(Vars.mysql){
			System.out.println("Preparing MySQL connection...");
			sql = new SQL(Vars.server, Vars.database, Vars.user, Vars.password, "3306", this);
		}else{
			System.out.println("Preparing SQLite connection...");
			sql = new SQL("data", this);
		}
		sql.connect();
		sql.update("CREATE TABLE IF NOT EXISTS points(UUID VARCHAR(64), score INT);");
		sql.update("CREATE TABLE IF NOT EXISTS stats(UUID VARCHAR(64), kills INT, deaths INT, time LONG);");
		
		PlayerManager.insert();
	}
	
}
