package me.MaxPlays.ZortixPvPShop.main;

import java.util.HashMap;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import me.MaxPlays.ZortixPvPShop.util.PlayerManager;
import me.MaxPlays.ZortixPvPShop.util.TimeParser;

public class Timer extends BukkitRunnable{

	static int run = 0;
	public static HashMap<String, Long> map = new HashMap<>();
	
	public Timer(){
		runTaskTimer(Main.plugin, 0, 20);
	}
	
	@Override
	public void run() {
		if(run >= 15){
			for(Entry<String, Long> e: map.entrySet()){
				PlayerManager.addStats(e.getKey(), "time", e.getValue());
			}
			run = 0;
			map.clear();
		}
		for(Player p: Bukkit.getOnlinePlayers()){
			String uuid = p.getUniqueId().toString();
			long l = 0;
			if(map.containsKey(uuid)){
				l = map.get(uuid);
				map.remove(uuid);
			}
			map.put(uuid, l + 1);
		}
		run++;
	}

	public static String getTimeString(long seconds){
		return new TimeParser(seconds).parse();
	}

}
