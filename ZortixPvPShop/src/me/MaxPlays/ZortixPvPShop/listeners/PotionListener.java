package me.MaxPlays.ZortixPvPShop.listeners;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import me.MaxPlays.ZortixPvPShop.util.Stack;
import me.MaxPlays.ZortixPvPShop.util.Vars;

public class PotionListener implements Listener{

	@EventHandler
	public void onPotion(PlayerInteractEvent e){
		if(e.getAction().equals(Action.RIGHT_CLICK_AIR) | e.getAction().equals(Action.RIGHT_CLICK_BLOCK)){
			Player p = e.getPlayer();
			ItemStack is = p.getItemInHand();
			if(is != null && is.getType().equals(Material.POTION) && is.getItemMeta().getDisplayName().equals("�bHealth Upgrade")){
				e.setCancelled(true);
				
				int toHealth = 22;
				if(p.getMaxHealth() == 22 | p.getMaxHealth() == 23){
					toHealth = 24;
				}else if(p.getMaxHealth() == 24 | p.getMaxHealth() == 25){
					toHealth = 26;
				}
				
				if(p.getMaxHealth() < 26){
					p.setItemInHand(new Stack(p.getItemInHand()).setAmount(p.getItemInHand().getAmount() - 1).build());
					p.setMaxHealth(toHealth);
					p.playSound(p.getLocation(), Sound.LEVEL_UP, 1, 2);
				}else{
					p.sendMessage(Vars.replace(Vars.shopHealth, p.getName(), "3"));
					p.playSound(p.getLocation(), Sound.NOTE_BASS_DRUM, 1, 2);
				}
			
			}
		}
	}
	
}
