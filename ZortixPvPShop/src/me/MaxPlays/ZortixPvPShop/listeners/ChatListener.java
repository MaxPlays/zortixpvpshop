package me.MaxPlays.ZortixPvPShop.listeners;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.scheduler.BukkitRunnable;

import me.MaxPlays.ZortixPvPShop.main.Main;
import me.MaxPlays.ZortixPvPShop.util.AndFinder;
import me.MaxPlays.ZortixPvPShop.util.WordScanner;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.HoverEvent.Action;
import net.md_5.bungee.api.chat.TextComponent;

public class ChatListener implements Listener{

	static String[] find = {"�a", "�b", "�c", "�d", "�e", "�f", "�0", "�1", "�2", "�3", "�4", "�5", "�6", "�7", "�8", "�9"};
	
	@EventHandler
	public void onCHat(final AsyncPlayerChatEvent e){
		if(!e.getMessage().toLowerCase().contains("#points"))
			return;
		e.setCancelled(true);
		final Player p = e.getPlayer();
		final String msg = e.getMessage();
		
		new BukkitRunnable() {
			
			@Override
			public void run() {
				String replace = e.getFormat().replace("%1$s", p.getDisplayName()).replace("%2$s", "");
				String found = AndFinder.findAnd(replace);
				WordScanner s = new WordScanner(msg);
				TextComponent tc = new TextComponent("");
				int points = -1;
				while(s.hasNext()){
					String word = s.getNext();
					for(String str: find){
						if(word.toLowerCase().contains(str.toLowerCase())){
							found = AndFinder.findAnd(word);
							break;
						}
					}
						
					if(word.toLowerCase().contains("#points")){
						
						if(points == -1){
							ResultSet rs = Main.sql.query("SELECT score FROM points WHERE UUID='" + p.getUniqueId().toString() + "';");
							try {
								if(rs.next()){
									points = rs.getInt("score");
								}else{
									points = 0;
								}
							} catch (SQLException e) {
								e.printStackTrace();
							}
						
						}
						
						TextComponent tmp = new TextComponent();
						tmp.setText("�6[�d" + points + " �dpoints�6] " + found);
						tmp.setHoverEvent(new HoverEvent(Action.SHOW_TEXT, new ComponentBuilder("�7" + p.getName() + "'s points: �c" + points).create()));
						tc.addExtra(tmp);
					}else{
						tc.addExtra(found + word + " " + found);
					}
				}
				TextComponent t = new TextComponent();
				t.setText(replace);
				t.addExtra(tc);
				for(Player pl: Bukkit.getOnlinePlayers()){
					pl.spigot().sendMessage(t);
				}
				Bukkit.getConsoleSender().sendMessage(t.toLegacyText());
			}
		}.runTaskAsynchronously(Main.plugin);
		
	}
	
}
