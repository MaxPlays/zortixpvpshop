package me.MaxPlays.ZortixPvPShop.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import me.MaxPlays.ZortixPvPShop.util.PlayerManager;

public class DeathListener implements Listener {

	@EventHandler
	public void onDeath(PlayerDeathEvent e) {
		if (e.getEntity() instanceof Player) {
			Player p = (Player) e.getEntity();
			PlayerManager.addStats(p.getUniqueId().toString(), "deaths", 1);
			if (p.getKiller() instanceof Player) {
				Player killer = (Player) p.getKiller();
				PlayerManager.addStats(killer.getUniqueId().toString(), "kills", 1);
			}
		}
	}

}
