package me.MaxPlays.ZortixPvPShop.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import me.MaxPlays.ZortixPvPShop.util.PlayerManager;

public class JoinListener implements Listener{

	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		Player p = e.getPlayer();
		PlayerManager.insert(p.getUniqueId().toString());
	}
	
}
