package me.MaxPlays.ZortixPvPShop.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import me.MaxPlays.ZortixPvPShop.shop.Shop;

public class ClickListener implements Listener{

	@EventHandler
	public void onClick(InventoryClickEvent e){
		Player p = (Player) e.getWhoClicked();
		ItemStack is = e.getCurrentItem();
		if(e.getInventory() != null && e.getInventory().getTitle().equals("�cServer shop")){
			e.setCancelled(true);
			if(e.getView().getTopInventory().equals(e.getClickedInventory()))
				Shop.buy(is, p);
		}
	}
	
}
