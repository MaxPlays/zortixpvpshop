package me.MaxPlays.ZortixPvPShop.util;

public class WordScanner {

	private String[] s;
	private int length, current = 0;
	
	public WordScanner(String s){
		this.s = s.split(" ");
		this.length = this.s.length;
	}
	
	public boolean hasNext(){
		return (current + 1) <= length;
	}
	
	public String getNext(){
		current++;
		return s[current - 1];
	}
}
