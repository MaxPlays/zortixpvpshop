package me.MaxPlays.ZortixPvPShop.util;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionEffectType;

public class Brewery {

	private ItemStack is;
	private PotionMeta pm;
	
	public Brewery(int amount, PotionEffectType type, int subid){
		is = new ItemStack(Material.POTION, amount, (short)subid);
		pm = (PotionMeta) is.getItemMeta();
		pm.setMainEffect(type);
	}
	public Brewery setName(String name){
		pm.setDisplayName(name);
		return this;
	}
	public Brewery setPrice(int price){
		pm.setLore(buildLore("�aCosts �c" + price + " �apoints", "�7<Click here to buy this item>", null));
		return this;
	}
	private List<String> buildLore(String line1, String line2, String line3){

		List<String> lore = new ArrayList<String>();
		
		if(line1 != null){
			lore.add(line1);
		}
		if(line2 != null){
			lore.add(line2);
		}
		if(line3 != null){
			lore.add(line3);
		}
		
		return lore;
	}
	public ItemStack build(){
		is.setItemMeta(pm);
		return is;
	}
	
}
