package me.MaxPlays.ZortixPvPShop.util;

public class TimeParser {

	long seconds;
	
	public TimeParser(long seconds){
		this.seconds = seconds;
	}
	
	public String parse(){
		StringBuilder s = new StringBuilder();
		
		int minutes = 0, hours = 0, days = 0;
		
		while(seconds >= 60){
			seconds -= 60;
			minutes++;
		}
		while(minutes >= 60){
			minutes -= 60;
			hours++;
		}
		while(hours >= 24){
			hours -= 24;
			days++;
		}
		
		if(days > 0){
			s.append(days + (days > 1 ? " days " : " day "));
		}
		if(hours > 0){
			s.append(hours + (hours > 1 ? " hours " : " hour "));
		}
		if(minutes > 0){
			s.append(minutes + (minutes > 1 ? " minutes " : " minute "));
		}
		if(seconds > 0){
			s.append(seconds + (seconds > 1 ? " seconds" : " second"));
		}else if(days == 0 && hours == 0 && minutes == 0){
			s.append("0 seconds");
		}
		return s.toString();
	}
	
}
