package me.MaxPlays.ZortixPvPShop.util;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;

public class Vars {
	
	public static boolean mysql;
	public static String user, password, database, server;
	public static String prefix;
	public static String pointsOwn, pointsPlayer, pointsNotEnough, pointsSent, pointsReceived, pointsTakeSender, pointsTakeRecipient, pointsSetSender, pointsSetRecipient;
	public static String statsResetSender, statsResetRecipient;
	public static String shopHealth, shopNotOnline, shopRemoveHealthSender, shopRemoveHealthRecipient;
	
	public Vars(FileConfiguration cfg){
		user = cfg.getString("mysql.user");
		password = cfg.getString("mysql.password");
		database = cfg.getString("mysql.database");
		server = cfg.getString("mysql.server");
		mysql = cfg.getString("backend").equalsIgnoreCase("mysql");
		
		prefix = td(cfg.getString("messages.prefix") + "&7 ");
		
		pointsOwn = t(cfg.getString("messages.points.showOwn"));
		pointsPlayer = t(cfg.getString("messages.points.showPlayer"));
		pointsNotEnough = t(cfg.getString("messages.points.notEnough"));
		pointsSent = t(cfg.getString("messages.points.sent"));
		pointsReceived = t(cfg.getString("messages.points.received"));
		pointsTakeSender = t(cfg.getString("messages.points.takeSender"));
		pointsTakeRecipient = t(cfg.getString("messages.points.takeRecipient"));
		pointsSetSender = t(cfg.getString("messages.points.setSender"));
		pointsSetRecipient = t(cfg.getString("messages.points.setRecipient"));
		
		statsResetSender = t(cfg.getString("messages.stats.resetSender"));
		statsResetRecipient = t(cfg.getString("messages.stats.resetRecipient"));
		
		shopHealth = t(cfg.getString("messages.shop.healthBoost"));
		shopNotOnline = t(cfg.getString("messages.shop.playerNotOnline"));
		shopRemoveHealthSender = t(cfg.getString("messages.shop.healthRemoveSender"));
		shopRemoveHealthRecipient = t(cfg.getString("messages.shop.healthRemoveRecipient"));
	}
	
	private String t(String s){
		return prefix + ChatColor.translateAlternateColorCodes('&', s);
	}
	private String td(String s){
		return ChatColor.translateAlternateColorCodes('&', s);
	}
	
	public static String replace(String msg, String p, String value){
		return msg.replace("[player]", p).replace("[value]", value);
	}
}
