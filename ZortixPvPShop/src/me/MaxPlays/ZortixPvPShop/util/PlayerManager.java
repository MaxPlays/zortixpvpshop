package me.MaxPlays.ZortixPvPShop.util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import me.MaxPlays.ZortixPvPShop.main.Main;

public class PlayerManager {

	public static void insert(){
		new BukkitRunnable() {
			@Override
			public void run() {
				for(Player p: Bukkit.getOnlinePlayers()){
					try {
						if(!Main.sql.query("SELECT score FROM points WHERE UUID='" + p.getUniqueId().toString() + "';").next())
							Main.sql.update("INSERT INTO points VALUES('" + p.getUniqueId().toString() + "', 0);");
						if(!Main.sql.query("SELECT time FROM stats WHERE UUID='" + p.getUniqueId().toString() + "';").next())
							Main.sql.update("INSERT INTO stats VALUES('" + p.getUniqueId().toString() + "', 0, 0, 0);");
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}.runTaskAsynchronously(Main.plugin);
	}
	public static void insert(final String uuid){
		new BukkitRunnable() {
			@Override
			public void run() {
					try {
						if(!Main.sql.query("SELECT score FROM points WHERE UUID='" + uuid + "';").next())
							Main.sql.update("INSERT INTO points VALUES('" + uuid + "', 0);");
						if(!Main.sql.query("SELECT time FROM stats WHERE UUID='" + uuid + "';").next())
							Main.sql.update("INSERT INTO stats VALUES('" + uuid + "', 0, 0, 0);");
					} catch (SQLException e) {
						e.printStackTrace();
					}
			}
		}.runTaskAsynchronously(Main.plugin);
	}
	public static void insert(final String uuid, final after a){
		new BukkitRunnable() {
			@Override
			public void run() {
					try {
						if(!Main.sql.query("SELECT score FROM points WHERE UUID='" + uuid + "';").next())
							Main.sql.update("INSERT INTO points VALUES('" + uuid + "', 0);");
						if(!Main.sql.query("SELECT time FROM stats WHERE UUID='" + uuid + "';").next())
							Main.sql.update("INSERT INTO stats VALUES('" + uuid + "', 0, 0, 0);");
					} catch (SQLException e) {
						e.printStackTrace();
					}
					new BukkitRunnable() {
						
						@Override
						public void run() {
							a.runAfter();
						}
					}.runTask(Main.plugin);
			}
		}.runTaskAsynchronously(Main.plugin);
	}
	public interface after{
		public void runAfter();
	}
	
	public static void getResults(final String uuid, final callback c){
		new BukkitRunnable() {
			
			@Override
			public void run() {
				ResultSet stats = Main.sql.query("SELECT * FROM stats WHERE UUID='" + uuid + "';");
				ResultSet points = Main.sql.query("SELECT score FROM points WHERE UUID='" + uuid + "';");
				
				HashMap<String, String> result = new HashMap<>();
				
				try{
					if(stats.next()){
						result.put("kills", String.valueOf(stats.getInt("kills")));
						result.put("deaths", String.valueOf(stats.getInt("deaths")));
						result.put("time", String.valueOf(stats.getInt("time")));
					}else{
						result.put("kills", "N/A");
						result.put("deaths", "N/A");
						result.put("time", "0");
					}
					if(points.next()){
						result.put("points", String.valueOf(points.getInt("score")));
					}else{
						result.put("points", "N/A");
					}
					c.onResult(result);
				}catch(SQLException e){
					e.printStackTrace();
					c.onResult(null);
				}
			}
		}.runTaskAsynchronously(Main.plugin);
	}
	
	public interface callback{
		public void onResult(HashMap<String, String> result);
	}
	public static void updateStats(String uuid, String col, int value){
		Main.sql.update("UPDATE stats SET " + col.toLowerCase() + "=" + value + " WHERE UUID='" + uuid + "';");
	}
	public static void updateStats(String uuid, String col, long value){
		Main.sql.update("UPDATE stats SET " + col.toLowerCase() + "=" + value + " WHERE UUID='" + uuid + "';");
	}
	public static void updatePoints(String uuid, int value){
		Main.sql.update("UPDATE points SET score=" + value + " WHERE UUID='" + uuid + "';");
	}
	public static void addStats(final String uuid, final String col, final int value){
		getResults(uuid, new callback() {
			@Override
			public void onResult(HashMap<String, String> result) {
				if(result != null){
					int i = 0;
					if(!result.get(col.toLowerCase()).equals("N/A"))
						i = Integer.valueOf(result.get(col.toLowerCase()));
					i += value;
					if(i >= 0){
						updateStats(uuid, col, i);
					}else{
						updateStats(uuid, col, Integer.MAX_VALUE);
					}
				}
			}
		});
	}
	public static void addStats(final String uuid, final String col, final long value){
		getResults(uuid, new callback() {
			@Override
			public void onResult(HashMap<String, String> result) {
				if(result != null){
					long i = 0;
					if(!result.get(col.toLowerCase()).equals("N/A"))
						i = Integer.valueOf(result.get(col.toLowerCase()));
					i += value;
					if(i >= 0){
						updateStats(uuid, col, i);
					}else{
						updateStats(uuid, col, Long.MAX_VALUE);
					}
				}
			}
		});
	}
	public static void removeStats(final String uuid, final String col, final int value){
		getResults(uuid, new callback() {
			@Override
			public void onResult(HashMap<String, String> result) {
				if(result != null){
					int i = 0;
					if(!result.get(col.toLowerCase()).equals("N/A"))
						i = Integer.valueOf(result.get(col.toLowerCase()));
					i -= value;
					if(i < 0)
						i = 0;
					updateStats(uuid, col, i);
				}
			}
		});
	}
	public static void addPoints(final String uuid, final int value){
		getResults(uuid, new callback() {
			@Override
			public void onResult(HashMap<String, String> result) {
				if(result != null){
					int i = 0;
					if(!result.get("points").equals("N/A"))
						i = Integer.valueOf(result.get("points"));
					i += value;
					if(i >= 0){
						updatePoints(uuid, i);
					}else{
						updatePoints(uuid, Integer.MAX_VALUE);
					}
				}
			}
		});
	}
	
	public static void removePoints(final String uuid, final int value){
		getResults(uuid, new callback() {
			@Override
			public void onResult(HashMap<String, String> result) {
				if(result != null){
					int i = 0;
					if(!result.get("points").equals("N/A"))
						i = Integer.valueOf(result.get("points"));
					i -= value;
					if(i < 0)
						i = 0;
					updatePoints(uuid, i);
				}
			}
		});
	}
}
