package me.MaxPlays.ZortixPvPShop.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.MaxPlays.ZortixPvPShop.shop.Shop;
import me.MaxPlays.ZortixPvPShop.util.Vars;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.HoverEvent.Action;
import net.md_5.bungee.api.chat.TextComponent;

public class CommandShop implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(sender instanceof Player && cmd.getName().equalsIgnoreCase("shop")){
			Player p = (Player) sender;
			if(args.length == 0){
				Shop.openGUI(p);
			}else if(args.length == 3){
				if(args[0].equalsIgnoreCase("removeheart") && p.hasPermission("shop.admin")){
					Player target = Bukkit.getPlayer(args[1]);
					if(target != null){
						int i = 0;
						try{
							i = Integer.valueOf(args[2]) * 2;
						}catch(Exception e){
							p.sendMessage(Vars.prefix + "The number you provided was in an invalid format. Use �c/points help �7for further information");
							return true;
						}
						if((target.getMaxHealth() - i) <= 1)
							i = 0;
						target.setMaxHealth(target.getMaxHealth() - (double)i);
						p.sendMessage(Vars.replace(Vars.shopRemoveHealthSender, target.getName(), String.valueOf(i / 2)));
						target.sendMessage(Vars.replace(Vars.shopRemoveHealthRecipient, p.getName(), String.valueOf(i / 2)));
					}else{
						p.sendMessage(Vars.replace(Vars.shopNotOnline, args[1], "N/A"));
					}
				}else{
					sendHelp(p);
				}
			}else{
				sendHelp(p);
			}
		}
		return true;
	}
	private void sendHelp(Player p){
		p.sendMessage("�8------------- [�cShop�8] -------------");
		p.sendMessage("�c/shop help �8......... �7Shows this help screen");
		p.sendMessage("�c/shop �8......... �7Opens the server shop");
		if(p.hasPermission("shop.admin"))
			p.sendMessage("�c/shop removeheart <Player> <Amount of hearts> �8......... Remove extra hearts from a player");
		
		TextComponent tc = new TextComponent("�7Plugin by ");
		TextComponent max = new TextComponent("�aMax_Plays");
		max.setHoverEvent(new HoverEvent(Action.SHOW_TEXT, new ComponentBuilder("�cClick to get to Bukkit page").create()));
		max.setClickEvent(new ClickEvent(net.md_5.bungee.api.chat.ClickEvent.Action.OPEN_URL, "https://bukkit.org/members/max8801.90728794/"));
		tc.addExtra(max);
		p.spigot().sendMessage(tc);
		
	}
}
