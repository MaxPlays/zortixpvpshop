package me.MaxPlays.ZortixPvPShop.commands;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import me.MaxPlays.ZortixPvPShop.main.Main;
import me.MaxPlays.ZortixPvPShop.util.PlayerManager;
import me.MaxPlays.ZortixPvPShop.util.PlayerManager.after;
import me.MaxPlays.ZortixPvPShop.util.PlayerManager.callback;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.HoverEvent.Action;
import me.MaxPlays.ZortixPvPShop.util.Vars;

public class CommandPoints implements CommandExecutor {

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, final String[] args) {
		
		if(sender instanceof Player && cmd.getName().equalsIgnoreCase("points")){
			final Player p = (Player) sender;
			if(args.length == 0){
				PlayerManager.getResults(p.getUniqueId().toString(), new callback() {
					
					@Override
					public void onResult(HashMap<String, String> result) {
						p.sendMessage(Vars.replace(Vars.pointsOwn, p.getName(), result.get("points")));
					}
				});
			}else if(args.length == 1){
				if(args[0].equalsIgnoreCase("help")){
					sendHelp(p);
				}else{
					new BukkitRunnable() {
						
						@Override
						public void run() {
							final String uuid = Bukkit.getOfflinePlayer(args[0]).getUniqueId().toString();
							PlayerManager.getResults(uuid, new callback() {
								
								@Override
								public void onResult(HashMap<String, String> result) {
									p.sendMessage(Vars.replace(Vars.pointsPlayer, args[0], result.get("points")));
								}
							});
						}
					}.runTaskAsynchronously(Main.plugin);	
				}
			}else if(args.length == 3){
				new BukkitRunnable() {
					
					@Override
					public void run() {
						final String uuid = Bukkit.getOfflinePlayer(args[1]).getUniqueId().toString();
						int i = 0;
						try{
							i = Integer.valueOf(args[2]);
						}catch(Exception e){
							p.sendMessage(Vars.prefix + "The number you provided was in an invalid format. Use �c/points help �7for further information");
							return;
						}
						if(i < 0 | i > Integer.MAX_VALUE){
							p.sendMessage(Vars.prefix + "The number you provided was in an invalid format. Use �c/points help �7for further information");
							return;
						}
						final int value = i;
						if(args[0].equalsIgnoreCase("transfer")){
							
							PlayerManager.getResults(p.getUniqueId().toString(), new callback() {
								
								@Override
								public void onResult(HashMap<String, String> result) {
									int points = Integer.valueOf(result.get("points"));
									if(points >= value){
										PlayerManager.insert(uuid, new after() {
											
											@Override
											public void runAfter() {
												PlayerManager.removePoints(p.getUniqueId().toString(), value);
												PlayerManager.addPoints(uuid, value);
												p.sendMessage(Vars.replace(Vars.pointsSent, args[1], String.valueOf(value)));
												if(Bukkit.getPlayer(args[1]) != null)
													Bukkit.getPlayer(args[1]).sendMessage(Vars.replace(Vars.pointsReceived, p.getName(), String.valueOf(value)));
											}
										});
									}else{
										p.sendMessage(Vars.replace(Vars.pointsNotEnough, args[1], String.valueOf(value)));
									}
								}
							});
						}else{
							if(p.hasPermission("shop.admin")){
								PlayerManager.insert(uuid, new after() {
									
									@Override
									public void runAfter() {
										if(args[0].equalsIgnoreCase("add")){
											PlayerManager.addPoints(uuid, value);
											p.sendMessage(Vars.replace(Vars.pointsSent, args[1], String.valueOf(value)));
											if(Bukkit.getPlayer(args[1]) != null)
												Bukkit.getPlayer(args[1]).sendMessage(Vars.replace(Vars.pointsReceived, p.getName(), String.valueOf(value)));
										}else if(args[0].equalsIgnoreCase("remove")){
											PlayerManager.removePoints(uuid, value);
											p.sendMessage(Vars.replace(Vars.pointsTakeSender, args[1], String.valueOf(value)));
											if(Bukkit.getPlayer(args[1]) != null)
												Bukkit.getPlayer(args[1]).sendMessage(Vars.replace(Vars.pointsTakeRecipient, p.getName(), String.valueOf(value)));
										}else if(args[0].equalsIgnoreCase("set")){
											PlayerManager.updatePoints(uuid, value);
											p.sendMessage(Vars.replace(Vars.pointsSetSender, args[1], String.valueOf(value)));
											if(Bukkit.getPlayer(args[1]) != null)
												Bukkit.getPlayer(args[1]).sendMessage(Vars.replace(Vars.pointsSetRecipient, p.getName(), String.valueOf(value)));
										}else{
											sendHelp(p);
										}
									}
								});
							}else{
								sendHelp(p);
							}
						}
					}
				}.runTaskAsynchronously(Main.plugin);
			}else{
				sendHelp(p);
			}
		}else if(sender instanceof ConsoleCommandSender && cmd.getName().equalsIgnoreCase("points")){
			final ConsoleCommandSender p = (ConsoleCommandSender) sender;
			if(args.length == 0){
				p.sendMessage("�cThe console can't do that");
			}else if(args.length == 1){
				if(args[0].equalsIgnoreCase("help")){
					sendHelp(p);
				}else{
					new BukkitRunnable() {
						
						@Override
						public void run() {
							final String uuid = Bukkit.getOfflinePlayer(args[0]).getUniqueId().toString();
							PlayerManager.getResults(uuid, new callback() {
								
								@Override
								public void onResult(HashMap<String, String> result) {
									p.sendMessage(Vars.replace(Vars.pointsPlayer, args[0], result.get("points")));
								}
							});
						}
					}.runTaskAsynchronously(Main.plugin);	
				}
			}else if(args.length == 3){
				new BukkitRunnable() {
					
					@Override
					public void run() {
						final String uuid = Bukkit.getOfflinePlayer(args[1]).getUniqueId().toString();
						int i = 0;
						try{
							i = Integer.valueOf(args[2]);
						}catch(Exception e){
							p.sendMessage(Vars.prefix + "The number you provided was in an invalid format. Use �c/points help �7for further information");
							return;
						}
						if(i < 0 | i > Integer.MAX_VALUE){
							p.sendMessage(Vars.prefix + "The number you provided was in an invalid format. Use �c/points help �7for further information");
							return;
						}
						final int value = i;
						if(args[0].equalsIgnoreCase("transfer")){
							p.sendMessage("�cThe console can't do that");
						}else{
							if(p.hasPermission("shop.admin")){
								PlayerManager.insert(uuid, new after() {
									
									@Override
									public void runAfter() {
										if(args[0].equalsIgnoreCase("add")){
											PlayerManager.addPoints(uuid, value);
											p.sendMessage(Vars.replace(Vars.pointsSent, args[1], String.valueOf(value)));
											if(Bukkit.getPlayer(args[1]) != null)
												Bukkit.getPlayer(args[1]).sendMessage(Vars.replace(Vars.pointsReceived, p.getName(), String.valueOf(value)));
										}else if(args[0].equalsIgnoreCase("remove")){
											PlayerManager.removePoints(uuid, value);
											p.sendMessage(Vars.replace(Vars.pointsTakeSender, args[1], String.valueOf(value)));
											if(Bukkit.getPlayer(args[1]) != null)
												Bukkit.getPlayer(args[1]).sendMessage(Vars.replace(Vars.pointsTakeRecipient, p.getName(), String.valueOf(value)));
										}else if(args[0].equalsIgnoreCase("set")){
											PlayerManager.updatePoints(uuid, value);
											p.sendMessage(Vars.replace(Vars.pointsSetSender, args[1], String.valueOf(value)));
											if(Bukkit.getPlayer(args[1]) != null)
												Bukkit.getPlayer(args[1]).sendMessage(Vars.replace(Vars.pointsSetRecipient, p.getName(), String.valueOf(value)));
										}else{
											sendHelp(p);
										}
									}
								});
							}else{
								sendHelp(p);
							}
						}
					}
				}.runTaskAsynchronously(Main.plugin);
			}else{
				sendHelp(p);
			}
		}
		
		return true;
	}
	
	private void sendHelp(Player p){
		p.sendMessage("�8------------- [�cPoints�8] -------------");
		p.sendMessage("�c/points help �8......... �7Shows this help screen");
		p.sendMessage("�c/points �8......... �7Shows your points");
		p.sendMessage("�c/points <Player> �8......... �7Shows the points of a player");
		p.sendMessage("�c/points transfer <Player> <Amount> �8......... �7Transfers points to a player");
		if(p.hasPermission("shop.admin")){
			p.sendMessage("�c/points set <Player> <Amount> �8......... �7Change the points of a player");
			p.sendMessage("�c/points add <Player> <Amount> �8......... �7Add points to a player");
			p.sendMessage("�c/points remove <Player> <Amount> �8......... �7Take points from a player");
		}
		TextComponent tc = new TextComponent("�7Plugin by ");
		TextComponent max = new TextComponent("�aMax_Plays");
		max.setHoverEvent(new HoverEvent(Action.SHOW_TEXT, new ComponentBuilder("�cClick to get to Bukkit page").create()));
		max.setClickEvent(new ClickEvent(net.md_5.bungee.api.chat.ClickEvent.Action.OPEN_URL, "https://bukkit.org/members/max8801.90728794/"));
		tc.addExtra(max);
		p.spigot().sendMessage(tc);
	}
	private void sendHelp(ConsoleCommandSender p){
		p.sendMessage("�8------------- [�cPoints�8] -------------");
		p.sendMessage("�c/points help �8......... �7Shows this help screen");
		p.sendMessage("�c/points �8......... �7Shows your points");
		p.sendMessage("�c/points <Player> �8......... �7Shows the points of a player");
		p.sendMessage("�c/points transfer <Player> <Amount> �8......... �7Transfers points to a player");
		if(p.hasPermission("shop.admin")){
			p.sendMessage("�c/points set <Player> <Amount> �8......... �7Change the points of a player");
			p.sendMessage("�c/points add <Player> <Amount> �8......... �7Add points to a player");
			p.sendMessage("�c/points remove <Player> <Amount> �8......... �7Take points from a player");
		}
		p.sendMessage("�7Plugin by �aMax_Plays");
	}

}
