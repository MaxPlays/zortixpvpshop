package me.MaxPlays.ZortixPvPShop.commands;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import me.MaxPlays.ZortixPvPShop.main.Main;
import me.MaxPlays.ZortixPvPShop.main.Timer;
import me.MaxPlays.ZortixPvPShop.util.PlayerManager;
import me.MaxPlays.ZortixPvPShop.util.PlayerManager.callback;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.HoverEvent.Action;
import me.MaxPlays.ZortixPvPShop.util.Vars;

public class CommandStats implements CommandExecutor {

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, final String[] args) {
		
		if(sender instanceof Player && cmd.getName().equalsIgnoreCase("stats")){
			final Player p = (Player) sender;
			if(args.length == 0){
				sendStats(p, p.getName());
			}else if(args.length == 1){
				if(args[0].equalsIgnoreCase("help")){
					sendHelp(p);
				}else{
					sendStats(p, args[0]);
				}
			}else if(args.length == 2){
				if(p.hasPermission("shop.admin")){
					if(args[0].equalsIgnoreCase("reset")){
						new BukkitRunnable() {
							
							@Override
							public void run() {
								String uuid = Bukkit.getOfflinePlayer(args[1]).getUniqueId().toString();
								String player = Bukkit.getOfflinePlayer(args[1]).getName();
								if(Timer.map.containsKey(uuid))
									Timer.map.remove(uuid);
								Main.sql.update("DELETE FROM stats WHERE UUID='" + uuid + "';");
								PlayerManager.insert(uuid);
								p.sendMessage(Vars.replace(Vars.statsResetSender, player, "N/A"));
								if(Bukkit.getPlayer(player) != null)
									Bukkit.getPlayer(player).sendMessage(Vars.replace(Vars.statsResetRecipient, p.getName(), "N/A"));
							}
						}.runTaskAsynchronously(Main.plugin);
						
						
					}else{
						sendHelp(p);
					}
				}else{
					sendHelp(p);
				}
			}else{
				sendHelp(p);
			}
		}
		
		return true;
	}
	
	private void sendHelp(Player p){
		p.sendMessage("�8------------- [�cStats�8] -------------");
		p.sendMessage("�c/stats help �8......... �7Shows this help screen");
		p.sendMessage("�c/stats �8......... �7Shows your stats");
		p.sendMessage("�c/stats <Player> �8......... �7Shows the stats of a player");
		if(p.hasPermission("shop.admin"))
			p.sendMessage("�c/stats reset <Player> �8......... �7Resets the stats of a player");
		TextComponent tc = new TextComponent("�7Plugin by ");
		TextComponent max = new TextComponent("�aMax_Plays");
		max.setHoverEvent(new HoverEvent(Action.SHOW_TEXT, new ComponentBuilder("�cClick to get to Bukkit page").create()));
		max.setClickEvent(new ClickEvent(net.md_5.bungee.api.chat.ClickEvent.Action.OPEN_URL, "https://bukkit.org/members/max8801.90728794/"));
		tc.addExtra(max);
		p.spigot().sendMessage(tc);
	}
	@SuppressWarnings("deprecation")
	private void sendStats(final Player p, final String from){	
		new BukkitRunnable() {
			
			@Override
			public void run() {
				p.sendMessage(Vars.prefix + "�8�oQuerying Mojang...");
				final String uuid = Bukkit.getOfflinePlayer(from).getUniqueId().toString();
				final String player = Bukkit.getOfflinePlayer(from).getName();
				p.sendMessage(Vars.prefix + "�8�oQuerying database...");
				PlayerManager.getResults(uuid, new callback() {
					@Override
					public void onResult(HashMap<String, String> result) {
						p.sendMessage(Vars.prefix + "Showing results for input �c" + from);
						p.sendMessage(Vars.prefix + "Current name: �6" + player);
						p.sendMessage(Vars.prefix + "UUID: �8" + uuid);
						p.sendMessage(Vars.prefix + "Kills: �c" + result.get("kills"));
						p.sendMessage(Vars.prefix + "Deaths: �c" + result.get("deaths"));
						if(!result.get("kills").equals("N/A") && !result.get("deaths").equals("N/A")){
							p.sendMessage(Vars.prefix + "K/D: �c" + computeKD(Integer.valueOf(result.get("kills")), Integer.valueOf(result.get("deaths"))));
						}else{
							p.sendMessage(Vars.prefix + "K/D: �cN/A");
						}
						p.sendMessage(Vars.prefix + "Points: �c" + result.get("points"));
						p.sendMessage(Vars.prefix + "Time online: �6" + Timer.getTimeString(Long.valueOf(result.get("time")) + (Timer.map.containsKey(uuid) ? Timer.map.get(uuid) : 0)));
					}
				});
			}
		}.runTaskAsynchronously(Main.plugin);
	}
	private String computeKD(int kills, int deaths){
		if(deaths > 0){
			double tmp = (kills / deaths) * 100;
			tmp = Math.round(tmp) / 100;
			return String.valueOf(tmp);
		}
		return "N/A";
	}
}
