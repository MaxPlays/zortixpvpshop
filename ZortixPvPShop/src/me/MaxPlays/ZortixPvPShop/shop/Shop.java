package me.MaxPlays.ZortixPvPShop.shop;

import java.util.HashMap;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import me.MaxPlays.ZortixPvPShop.util.PlayerManager;
import me.MaxPlays.ZortixPvPShop.util.PlayerManager.callback;
import me.MaxPlays.ZortixPvPShop.util.Stack;
import me.MaxPlays.ZortixPvPShop.util.Vars;

public class Shop {

	
	private static Inventory inv;
	
	public Shop(){
		inv = Bukkit.createInventory(null, 45, "�cServer shop");
		for(Entry<Integer, ItemStack> e: Items.items.entrySet())
			inv.setItem(e.getKey(), e.getValue());
	}
	
	public static void openGUI(Player p){
		p.closeInventory();
		p.openInventory(inv);
		p.playSound(p.getLocation(), Sound.ITEM_PICKUP, 1, 2);
	}
	public static void buy(final ItemStack is, final Player p){
		if(is != null && is.getItemMeta() != null && is.hasItemMeta()){
			final int price = Integer.valueOf(is.getItemMeta().getLore().get(0).replace("�aCosts �c", "").replace(" �apoints", ""));
			PlayerManager.getResults(p.getUniqueId().toString(), new callback() {
				
				@Override
				public void onResult(HashMap<String, String> result) {
					int has = Integer.valueOf(result.get("points"));
					if(has >= price){
						PlayerManager.removePoints(p.getUniqueId().toString(), price);
						p.getInventory().addItem(new Stack(is).resetLore().build());
						p.playSound(p.getLocation(), Sound.NOTE_PLING, 1, 1);
					}else{
						p.sendMessage(Vars.replace(Vars.pointsNotEnough, p.getName(), String.valueOf(price - has)));
						p.playSound(p.getLocation(), Sound.NOTE_BASS_DRUM, 1, 1);
					}
				}
			});
		}else{
			p.playSound(p.getLocation(), Sound.NOTE_BASS_DRUM, 1, 1);
		}
	}
}
