package me.MaxPlays.ZortixPvPShop.shop;

import java.util.HashMap;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

import me.MaxPlays.ZortixPvPShop.util.Brewery;
import me.MaxPlays.ZortixPvPShop.util.Stack;

public class Items {

	public static HashMap<Integer, ItemStack> items = new HashMap<>();
	
	public Items(){
		registerItem(0, new Brewery(1, PotionEffectType.HEAL, 8197).setName("�bHealth Upgrade").setPrice(500).build());
		registerItem(1, new Stack(Material.GOLDEN_APPLE, 1, 1).setName("�bGod Apple").setPrice(5).build());
		registerItem(2, new Stack(Material.GOLDEN_APPLE, 10, 1).setName("�bGod Apple").setPrice(50).build());
		
		registerItem(16, new Stack(Material.DIAMOND_SWORD).setName("�eSword").addEnchantment(Enchantment.DAMAGE_ALL, 3)
				.addEnchantment(Enchantment.FIRE_ASPECT, 2).setPrice(2).build());
		registerItem(17, new Stack(Material.BOW).setName("�eWooden Bow").addEnchantment(Enchantment.ARROW_DAMAGE, 3)
				.setPrice(2).build());
		
		registerItem(18, new Stack(Material.DIAMOND_HELMET).setName("�eHelmet").setPrice(5)
				.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4).build());
		registerItem(19, new Stack(Material.DIAMOND_CHESTPLATE).setName("�eChestplate").setPrice(5)
				.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4).build());
		registerItem(20, new Stack(Material.DIAMOND_LEGGINGS).setName("�eLeggings").setPrice(5)
				.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4).build());
		registerItem(21, new Stack(Material.DIAMOND_BOOTS).setName("�eBoots").setPrice(5)
				.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4).build());
		registerItem(25, new Stack(Material.DIAMOND_SWORD).setName("�eSword").addEnchantment(Enchantment.DAMAGE_ALL, 4)
				.addEnchantment(Enchantment.FIRE_ASPECT, 2).setPrice(4).build());
		registerItem(26, new Stack(Material.BOW).setName("�eWooden Bow").addEnchantment(Enchantment.ARROW_DAMAGE, 4)
				.setPrice(4).addEnchantment(Enchantment.ARROW_FIRE, 1).build());
		
		registerItem(27, new Stack(Material.DIAMOND_HELMET).setName("�eHelmet").setPrice(12)
				.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4)
				.addEnchantment(Enchantment.DURABILITY, 3).build());
		registerItem(28, new Stack(Material.DIAMOND_CHESTPLATE).setName("�eChestplate").setPrice(12)
				.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4)
				.addEnchantment(Enchantment.DURABILITY, 3).build());
		registerItem(29, new Stack(Material.DIAMOND_LEGGINGS).setName("�eLeggings").setPrice(12)
				.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4)
				.addEnchantment(Enchantment.DURABILITY, 3).build());
		registerItem(30, new Stack(Material.DIAMOND_BOOTS).setName("�eBoots").setPrice(12)
				.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4)
				.addEnchantment(Enchantment.DURABILITY, 3).build());
		registerItem(34, new Stack(Material.DIAMOND_SWORD).setName("�eSword").addEnchantment(Enchantment.DAMAGE_ALL, 4)
				.addEnchantment(Enchantment.FIRE_ASPECT, 2).setPrice(8).addEnchantment(Enchantment.KNOCKBACK, 2).build());
		registerItem(35, new Stack(Material.BOW).setName("�eWooden Bow").addEnchantment(Enchantment.ARROW_DAMAGE, 4)
				.setPrice(8).addEnchantment(Enchantment.ARROW_FIRE, 1).addEnchantment(Enchantment.ARROW_INFINITE, 1).build());
		
		registerItem(36, new Brewery(1, PotionEffectType.SPEED, 8194).setName("�bSwiftness Potion").setPrice(5).build());
		registerItem(37, new Brewery(1, PotionEffectType.REGENERATION, 8193).setName("�bRegeneration Potion").setPrice(5).build());
		registerItem(38, new Brewery(1, PotionEffectType.WATER_BREATHING, 8205).setName("�bWater Breathing Potion").setPrice(5).build());
		registerItem(39, new Brewery(1, PotionEffectType.FIRE_RESISTANCE, 8195).setName("�bFire Resistance Potion").setPrice(5).build());
		registerItem(43, new Stack(Material.DIAMOND_SWORD).setName("�eSword").addEnchantment(Enchantment.DAMAGE_ALL, 4)
				.addEnchantment(Enchantment.FIRE_ASPECT, 2).setPrice(10).addEnchantment(Enchantment.KNOCKBACK, 2)
				.addEnchantment(Enchantment.DURABILITY, 3).build());
		registerItem(44, new Stack(Material.BOW).setName("�eWooden Bow").addEnchantment(Enchantment.ARROW_DAMAGE, 4)
				.setPrice(10).addEnchantment(Enchantment.ARROW_FIRE, 1).addEnchantment(Enchantment.ARROW_INFINITE, 1)
				.addEnchantment(Enchantment.ARROW_KNOCKBACK, 2).build());
	}
	
	public static void registerItem(int slot, ItemStack is){
		items.put(slot, is);
	}
	
}
